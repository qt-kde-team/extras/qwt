Source: qwt
Priority: optional
Maintainer: Gudjon I. Gudjonsson <gudjon@gudjon.org>
Uploaders: Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>
Build-Depends: debhelper-compat (= 13),
               libqt5opengl5-dev,
               libqt5svg5-dev,
               pkg-kde-tools,
               qtbase5-dev,
               qttools5-dev,
               qt6-base-dev,
               qt6-base-dev-tools,
               qt6-tools-dev,
               libqt6opengl6-dev,
               qt6-svg-dev
Standards-Version: 4.6.1
Section: libs
Homepage: https://qwt.sourceforge.net
Vcs-Git: https://salsa.debian.org/qt-kde-team/extras/qwt.git
Vcs-Browser: https://salsa.debian.org/qt-kde-team/extras/qwt

Package: libqwt-headers
Section: libdevel
Architecture: any
Depends: ${misc:Depends}
Suggests:
         libqwt-qt5-dev,
         libqwt-qt6-dev
Breaks: libqwt-qt5-dev (<< 6.2.0-1~)
Replaces: libqwt-qt5-dev (<< 6.2.0-1~)
Multi-Arch: foreign
Description: Qt widgets library for technical applications (header files)
 The Qwt library contains Qt GUI Components and utility classes which
 are primarily useful for programs with a technical background. Most of
 these widgets are used to control or display values, arrays, or ranges
 of type double.
 .
 This package contains the Qwt header files for Qt5 and Qt6.

Package: libqwt-qt5-dev
Section: libdevel
Architecture: any
Depends: libqwt-qt5-6.2 (= ${binary:Version}),
         libqwt-headers (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Multi-Arch: same
Description: Qt widgets library for technical applications (development, qt5)
 The Qwt library contains Qt GUI Components and utility classes which
 are primarily useful for programs with a technical background. Most of
 these widgets are used to control or display values, arrays, or ranges
 of type double.
 .
 This package contains the Qwt development files for Qt5.

Package: libqwt-qt6-dev
Section: libdevel
Architecture: any
Depends: libqwt-qt6-6.2 (= ${binary:Version}),
         libqwt-headers (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Multi-Arch: same
Description: Qt widgets library for technical applications (development, qt6)
 The Qwt library contains Qt GUI Components and utility classes which
 are primarily useful for programs with a technical background. Most of
 these widgets are used to control or display values, arrays, or ranges
 of type double.
 .
 This package contains the Qwt development files for Qt6.

Package: libqwt-qt5-6.2
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Breaks: libqwt-qt5-6 (<< 6.2.0-1~)
Replaces: libqwt-qt5-6 (<< 6.2.0-1~)
Multi-Arch: same
Description: Qt widgets library for technical applications (runtime, qt5)
 The Qwt library contains Qt GUI Components and utility classes which
 are primarily useful for programs with a technical background. Most of
 these widgets are used to control or display values, arrays, or ranges
 of type double.
 .
 This package contains the files necessary for running applications that
 use the Qwt library and Qt5.

Package: libqwt-qt6-6.2
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Description: Qt widgets library for technical applications (runtime, qt6)
 The Qwt library contains Qt GUI Components and utility classes which
 are primarily useful for programs with a technical background. Most of
 these widgets are used to control or display values, arrays, or ranges
 of type double.
 .
 This package contains the files necessary for running applications that
 use the Qwt library and Qt6.

Package: libqwt-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Recommends: w3m | www-browser
Multi-Arch: foreign
Description: Qt widgets library for technical applications (documentation)
 The Qwt library contains Qt GUI Components and utility classes which
 are primarily useful for programs with a technical background. Most of
 these widgets are used to control or display values, arrays, or ranges
 of type double.
 .
 This package contains the Qwt library documentation and programming examples
 for developers.
